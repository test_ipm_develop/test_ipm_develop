create or replace trigger DBIPM.I_PRE_TRADE_TX_AR
after insert or update or delete on I_PRE_TRADE_TX_TB  
for each row
declare
  G_PROD_ID               varchar2(30) := 'I_PRE_TRADE_TX_AR';
  T_SEQ                   DBIPM.I_PRE_TRADE_DAILY_TB.I_SEQ%type;
  T_TRADE_DATE            DBIPM.I_PRE_TRADE_PROD_TB.I_TRADE_DATE%type;
  T_PORTFOLIO_SEQ         DBIPM.I_PRE_TRADE_PROD_TB.I_PORTFOLIO_SEQ%type;
  T_BUY_AMT               DBIPM.I_PRE_TRADE_TX_TB.I_AMT_USD%type;
  T_SELL_AMT              DBIPM.I_PRE_TRADE_TX_TB.I_AMT_USD%type;
begin
  if inserting then
    if :NEW.I_STATUS = 'A' then
      select A.I_SEQ, A.I_TRADE_DATE, A.I_PORTFOLIO_SEQ
        into T_SEQ, T_TRADE_DATE, T_PORTFOLIO_SEQ
        from DBIPM.I_PRE_TRADE_DAILY_TB A,
             DBIPM.I_PRE_TRADE_MMA_TB B
       where A.I_TRADE_DATE = B.I_DATE
         and A.I_PORTFOLIO_SEQ = B.I_PORTFOLIO_SEQ
         and B.I_PRE_TRADE_MMA_SEQ = :NEW.I_PRE_TRADE_MMA_SEQ;
      
      if (DBIPM.I_PRE_TRADE_PG.I_BUY_SELL_FC(:NEW.I_PROD_TYPE, :NEW.I_INVEST_PROD_CODE, :NEW.I_TRADE_TYPE) = 'BUY') then
        T_BUY_AMT := :NEW.I_AMT_USD;
        T_SELL_AMT := 0;
      elsif (DBIPM.I_PRE_TRADE_PG.I_BUY_SELL_FC(:NEW.I_PROD_TYPE, :NEW.I_INVEST_PROD_CODE, :NEW.I_TRADE_TYPE) = 'SELL') then
        T_BUY_AMT := 0;
        T_SELL_AMT := :NEW.I_AMT_USD;
      else
        T_BUY_AMT := 0;
        T_SELL_AMT := 0;
      end if;
      
      update DBIPM.I_PRE_TRADE_DAILY_TB A
         set A.I_BUY_VOLUME = A.I_BUY_VOLUME + T_BUY_AMT,
             A.I_SELL_VOLUME = A.I_SELL_VOLUME + T_SELL_AMT,
             A.I_TOT_VOLUME = A.I_TOT_VOLUME + :NEW.I_AMT_USD,
             A.I_AMEND_PROG = G_PROD_ID,
             A.I_AMEND_USER = user,
             A.I_AMEND_DATE = sysdate
       where A.I_SEQ = T_SEQ;
      
      update DBIPM.I_PRE_TRADE_PROD_TB A
         set A.I_TOTAL_QTY = A.I_TOTAL_QTY + :NEW.I_QTY,
             A.I_TOTAL_AMT = A.I_TOTAL_AMT + :NEW.I_AMT,
             A.I_TOTAL_AMT_USD = A.I_TOTAL_AMT_USD + :NEW.I_AMT_USD,
             A.I_AMEND_PROG = G_PROD_ID,
             A.I_AMEND_USER = user,
             A.I_AMEND_DATE = sysdate
       where A.I_TRADE_DATE = T_TRADE_DATE
         and A.I_PORTFOLIO_SEQ = T_PORTFOLIO_SEQ
         and A.I_TRADE_TYPE = :NEW.I_TRADE_TYPE
         and A.I_PROD_TYPE = :NEW.I_PROD_TYPE
         and A.I_INVEST_PROD_CODE = :NEW.I_INVEST_PROD_CODE;
      
      update DBIPM.I_PRE_TRADE_MMA_TB A
         set A.I_BUY_VOLUME = A.I_BUY_VOLUME + T_BUY_AMT,
             A.I_SELL_VOLUME = A.I_SELL_VOLUME + T_SELL_AMT,
             A.I_TOT_VOLUME = A.I_TOT_VOLUME + :NEW.I_AMT_USD,
             A.I_AMEND_PROG = G_PROD_ID,
             A.I_AMEND_USER = user,
             A.I_AMEND_DATE = sysdate
       where A.I_PRE_TRADE_MMA_SEQ = :NEW.I_PRE_TRADE_MMA_SEQ;
    end if;
  end if;
  
  if updating then
    if :NEW.I_STATUS = 'A' and :OLD.I_STATUS = 'A' and
      (:NEW.I_AMT_USD <> :OLD.I_AMT_USD or :NEW.I_PRE_TRADE_MMA_SEQ <> :OLD.I_PRE_TRADE_MMA_SEQ) then
      --OLD
      select A.I_SEQ, A.I_TRADE_DATE, A.I_PORTFOLIO_SEQ
        into T_SEQ, T_TRADE_DATE, T_PORTFOLIO_SEQ
        from DBIPM.I_PRE_TRADE_DAILY_TB A,
             DBIPM.I_PRE_TRADE_MMA_TB B
       where A.I_TRADE_DATE = B.I_DATE
         and A.I_PORTFOLIO_SEQ = B.I_PORTFOLIO_SEQ
         and B.I_PRE_TRADE_MMA_SEQ = :OLD.I_PRE_TRADE_MMA_SEQ;
      
      if (DBIPM.I_PRE_TRADE_PG.I_BUY_SELL_FC(:OLD.I_PROD_TYPE, :OLD.I_INVEST_PROD_CODE, :OLD.I_TRADE_TYPE) = 'BUY') then
        T_BUY_AMT := :OLD.I_AMT_USD;
        T_SELL_AMT := 0;
      elsif (DBIPM.I_PRE_TRADE_PG.I_BUY_SELL_FC(:OLD.I_PROD_TYPE, :OLD.I_INVEST_PROD_CODE, :OLD.I_TRADE_TYPE) = 'SELL') then
        T_BUY_AMT := 0;
        T_SELL_AMT := :OLD.I_AMT_USD;
      else
        T_BUY_AMT := 0;
        T_SELL_AMT := 0;
      end if;
      
      update DBIPM.I_PRE_TRADE_DAILY_TB A
         set A.I_BUY_VOLUME = A.I_BUY_VOLUME - T_BUY_AMT,
             A.I_SELL_VOLUME = A.I_SELL_VOLUME - T_SELL_AMT,
             A.I_TOT_VOLUME = A.I_TOT_VOLUME - :OLD.I_AMT_USD,
             A.I_AMEND_PROG = G_PROD_ID,
             A.I_AMEND_USER = user,
             A.I_AMEND_DATE = sysdate
       where A.I_SEQ = T_SEQ;
      
      update DBIPM.I_PRE_TRADE_PROD_TB A
         set A.I_TOTAL_QTY = A.I_TOTAL_QTY - :OLD.I_QTY,
             A.I_TOTAL_AMT = A.I_TOTAL_AMT - :OLD.I_AMT,
             A.I_TOTAL_AMT_USD = A.I_TOTAL_AMT_USD - :OLD.I_AMT_USD,
             A.I_AMEND_PROG = G_PROD_ID,
             A.I_AMEND_USER = user,
             A.I_AMEND_DATE = sysdate
       where A.I_TRADE_DATE = T_TRADE_DATE
         and A.I_PORTFOLIO_SEQ = T_PORTFOLIO_SEQ
         and A.I_TRADE_TYPE = :OLD.I_TRADE_TYPE
         and A.I_PROD_TYPE = :OLD.I_PROD_TYPE
         and A.I_INVEST_PROD_CODE = :OLD.I_INVEST_PROD_CODE;
      
      update DBIPM.I_PRE_TRADE_MMA_TB A
         set A.I_BUY_VOLUME = A.I_BUY_VOLUME - T_BUY_AMT,
             A.I_SELL_VOLUME = A.I_SELL_VOLUME - T_SELL_AMT,
             A.I_TOT_VOLUME = A.I_TOT_VOLUME - :OLD.I_AMT_USD,
             A.I_AMEND_PROG = G_PROD_ID,
             A.I_AMEND_USER = user,
             A.I_AMEND_DATE = sysdate
       where A.I_PRE_TRADE_MMA_SEQ = :OLD.I_PRE_TRADE_MMA_SEQ;
      
      --NEW
      select A.I_SEQ, A.I_TRADE_DATE, A.I_PORTFOLIO_SEQ
        into T_SEQ, T_TRADE_DATE, T_PORTFOLIO_SEQ
        from DBIPM.I_PRE_TRADE_DAILY_TB A,
             DBIPM.I_PRE_TRADE_MMA_TB B
       where A.I_TRADE_DATE = B.I_DATE
         and A.I_PORTFOLIO_SEQ = B.I_PORTFOLIO_SEQ
         and B.I_PRE_TRADE_MMA_SEQ = :NEW.I_PRE_TRADE_MMA_SEQ;
      
      if (DBIPM.I_PRE_TRADE_PG.I_BUY_SELL_FC(:NEW.I_PROD_TYPE, :NEW.I_INVEST_PROD_CODE, :NEW.I_TRADE_TYPE) = 'BUY') then
        T_BUY_AMT := :NEW.I_AMT_USD;
        T_SELL_AMT := 0;
      elsif (DBIPM.I_PRE_TRADE_PG.I_BUY_SELL_FC(:NEW.I_PROD_TYPE, :NEW.I_INVEST_PROD_CODE, :NEW.I_TRADE_TYPE) = 'SELL') then
        T_BUY_AMT := 0;
        T_SELL_AMT := :NEW.I_AMT_USD;
      else
        T_BUY_AMT := 0;
        T_SELL_AMT := 0;
      end if;
      
      update DBIPM.I_PRE_TRADE_DAILY_TB A
         set A.I_BUY_VOLUME = A.I_BUY_VOLUME + T_BUY_AMT,
             A.I_TOT_VOLUME = A.I_TOT_VOLUME + :NEW.I_AMT_USD,
             A.I_AMEND_PROG = G_PROD_ID,
             A.I_AMEND_USER = user,
             A.I_AMEND_DATE = sysdate
       where A.I_SEQ = T_SEQ;
      
      update DBIPM.I_PRE_TRADE_PROD_TB A
         set A.I_TOTAL_QTY = A.I_TOTAL_QTY + :NEW.I_QTY,
             A.I_TOTAL_AMT = A.I_TOTAL_AMT + :NEW.I_AMT,
             A.I_TOTAL_AMT_USD = A.I_TOTAL_AMT_USD + :NEW.I_AMT_USD,
             A.I_AMEND_PROG = G_PROD_ID,
             A.I_AMEND_USER = user,
             A.I_AMEND_DATE = sysdate
       where A.I_TRADE_DATE = T_TRADE_DATE
         and A.I_PORTFOLIO_SEQ = T_PORTFOLIO_SEQ
         and A.I_TRADE_TYPE = :NEW.I_TRADE_TYPE
         and A.I_PROD_TYPE = :NEW.I_PROD_TYPE
         and A.I_INVEST_PROD_CODE = :NEW.I_INVEST_PROD_CODE;
      
      update DBIPM.I_PRE_TRADE_MMA_TB A
         set A.I_BUY_VOLUME = A.I_BUY_VOLUME + T_BUY_AMT,
             A.I_SELL_VOLUME = A.I_SELL_VOLUME + T_SELL_AMT,
             A.I_TOT_VOLUME = A.I_TOT_VOLUME + :NEW.I_AMT_USD,
             A.I_AMEND_PROG = G_PROD_ID,
             A.I_AMEND_USER = user,
             A.I_AMEND_DATE = sysdate
       where A.I_PRE_TRADE_MMA_SEQ = :NEW.I_PRE_TRADE_MMA_SEQ;
    elsif :NEW.I_STATUS = 'D' and :OLD.I_STATUS = 'A' then
      select A.I_SEQ, A.I_TRADE_DATE, A.I_PORTFOLIO_SEQ
        into T_SEQ, T_TRADE_DATE, T_PORTFOLIO_SEQ
        from DBIPM.I_PRE_TRADE_DAILY_TB A,
             DBIPM.I_PRE_TRADE_MMA_TB B
       where A.I_TRADE_DATE = B.I_DATE
         and A.I_PORTFOLIO_SEQ = B.I_PORTFOLIO_SEQ
         and B.I_PRE_TRADE_MMA_SEQ = :OLD.I_PRE_TRADE_MMA_SEQ;
      
      if (DBIPM.I_PRE_TRADE_PG.I_BUY_SELL_FC(:OLD.I_PROD_TYPE, :OLD.I_INVEST_PROD_CODE, :OLD.I_TRADE_TYPE) = 'BUY') then
        T_BUY_AMT := :OLD.I_AMT_USD;
        T_SELL_AMT := 0;
      elsif (DBIPM.I_PRE_TRADE_PG.I_BUY_SELL_FC(:OLD.I_PROD_TYPE, :OLD.I_INVEST_PROD_CODE, :OLD.I_TRADE_TYPE) = 'SELL') then
        T_BUY_AMT := 0;
        T_SELL_AMT := :OLD.I_AMT_USD;
      else
        T_BUY_AMT := 0;
        T_SELL_AMT := 0;
      end if;
      
      update DBIPM.I_PRE_TRADE_DAILY_TB A
         set A.I_BUY_VOLUME = A.I_BUY_VOLUME - T_BUY_AMT,
             A.I_SELL_VOLUME = A.I_SELL_VOLUME - T_SELL_AMT,
             A.I_TOT_VOLUME = A.I_TOT_VOLUME - :OLD.I_AMT_USD,
             A.I_AMEND_PROG = G_PROD_ID,
             A.I_AMEND_USER = user,
             A.I_AMEND_DATE = sysdate
       where A.I_SEQ = T_SEQ;
      
      update DBIPM.I_PRE_TRADE_PROD_TB A
         set A.I_TOTAL_QTY = A.I_TOTAL_QTY - :OLD.I_QTY,
             A.I_TOTAL_AMT = A.I_TOTAL_AMT - :OLD.I_AMT,
             A.I_TOTAL_AMT_USD = A.I_TOTAL_AMT_USD - :OLD.I_AMT_USD,
             A.I_AMEND_PROG = G_PROD_ID,
             A.I_AMEND_USER = user,
             A.I_AMEND_DATE = sysdate
       where A.I_TRADE_DATE = T_TRADE_DATE
         and A.I_PORTFOLIO_SEQ = T_PORTFOLIO_SEQ
         and A.I_TRADE_TYPE = :OLD.I_TRADE_TYPE
         and A.I_PROD_TYPE = :OLD.I_PROD_TYPE
         and A.I_INVEST_PROD_CODE = :OLD.I_INVEST_PROD_CODE;
      
      update DBIPM.I_PRE_TRADE_MMA_TB A
         set A.I_BUY_VOLUME = A.I_BUY_VOLUME - T_BUY_AMT,
             A.I_SELL_VOLUME = A.I_SELL_VOLUME - T_SELL_AMT,
             A.I_TOT_VOLUME = A.I_TOT_VOLUME - :OLD.I_AMT_USD,
             A.I_AMEND_PROG = G_PROD_ID,
             A.I_AMEND_USER = user,
             A.I_AMEND_DATE = sysdate
       where A.I_PRE_TRADE_MMA_SEQ = :OLD.I_PRE_TRADE_MMA_SEQ;
    end if;
  end if;
  
  if deleting then
    select A.I_SEQ, A.I_TRADE_DATE, A.I_PORTFOLIO_SEQ
      into T_SEQ, T_TRADE_DATE, T_PORTFOLIO_SEQ
      from DBIPM.I_PRE_TRADE_DAILY_TB A,
           DBIPM.I_PRE_TRADE_MMA_TB B
     where A.I_TRADE_DATE = B.I_DATE
       and A.I_PORTFOLIO_SEQ = B.I_PORTFOLIO_SEQ
       and B.I_PRE_TRADE_MMA_SEQ = :OLD.I_PRE_TRADE_MMA_SEQ;
    
    if (DBIPM.I_PRE_TRADE_PG.I_BUY_SELL_FC(:OLD.I_PROD_TYPE, :OLD.I_INVEST_PROD_CODE, :OLD.I_TRADE_TYPE) = 'BUY') then
        T_BUY_AMT := :OLD.I_AMT_USD;
        T_SELL_AMT := 0;
      elsif (DBIPM.I_PRE_TRADE_PG.I_BUY_SELL_FC(:OLD.I_PROD_TYPE, :OLD.I_INVEST_PROD_CODE, :OLD.I_TRADE_TYPE) = 'SELL') then
        T_BUY_AMT := 0;
        T_SELL_AMT := :OLD.I_AMT_USD;
      else
        T_BUY_AMT := 0;
        T_SELL_AMT := 0;
      end if;
    
    update DBIPM.I_PRE_TRADE_DAILY_TB A
       set A.I_BUY_VOLUME = A.I_BUY_VOLUME - T_BUY_AMT,
           A.I_SELL_VOLUME = A.I_SELL_VOLUME - T_SELL_AMT,
           A.I_TOT_VOLUME = A.I_TOT_VOLUME - :OLD.I_AMT_USD,
             A.I_AMEND_PROG = G_PROD_ID,
             A.I_AMEND_USER = user,
             A.I_AMEND_DATE = sysdate
     where A.I_SEQ = T_SEQ;
    
    update DBIPM.I_PRE_TRADE_PROD_TB A
       set A.I_TOTAL_QTY = A.I_TOTAL_QTY - :OLD.I_QTY,
           A.I_TOTAL_AMT = A.I_TOTAL_AMT - :OLD.I_AMT,
           A.I_TOTAL_AMT_USD = A.I_TOTAL_AMT_USD - :OLD.I_AMT_USD,
             A.I_AMEND_PROG = G_PROD_ID,
             A.I_AMEND_USER = user,
             A.I_AMEND_DATE = sysdate
     where A.I_TRADE_DATE = T_TRADE_DATE
       and A.I_PORTFOLIO_SEQ = T_PORTFOLIO_SEQ
       and A.I_TRADE_TYPE = :OLD.I_TRADE_TYPE
       and A.I_PROD_TYPE = :OLD.I_PROD_TYPE
       and A.I_INVEST_PROD_CODE = :OLD.I_INVEST_PROD_CODE;
    
    update DBIPM.I_PRE_TRADE_MMA_TB A
       set A.I_BUY_VOLUME = A.I_BUY_VOLUME - T_BUY_AMT,
           A.I_SELL_VOLUME = A.I_SELL_VOLUME - T_SELL_AMT,
           A.I_TOT_VOLUME = A.I_TOT_VOLUME - :OLD.I_AMT_USD,
             A.I_AMEND_PROG = G_PROD_ID,
             A.I_AMEND_USER = user,
             A.I_AMEND_DATE = sysdate
     where A.I_PRE_TRADE_MMA_SEQ = :OLD.I_PRE_TRADE_MMA_SEQ;
  end if;
end I_PRE_TRADE_TX_AR;
/

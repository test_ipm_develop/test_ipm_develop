create or replace trigger I_PRE_TRADE_TX_BR
before insert or update or delete on I_PRE_TRADE_TX_TB  
for each row
declare
  G_SYS_CODE              DBGLC.G_ERROR_DESC_TB.G_SYSTEM_CODE%type := 'IPM';
  
  T_COUNT                 number;
  T_BAL_AMT               number;
  T_MMA_DATE              DBIPM.I_PRE_TRADE_MMA_TB.I_DATE%type;
begin
  if inserting or updating then
    select A.I_DATE
      into T_MMA_DATE
      from DBIPM.I_PRE_TRADE_MMA_TB A
     where A.I_PRE_TRADE_MMA_SEQ = :NEW.I_PRE_TRADE_MMA_SEQ;
    if :NEW.I_GOOD_TILL_DATE < T_MMA_DATE then
      raise_application_error(-20270, DBGLC.G_ERROR_MSG_FC(G_SYS_CODE, -20270, 'Good Till Date', 'Pre-Trade Date'));
    end if;
    
    if :NEW.I_PROD_TYPE = 'IRSW' then
      if :NEW.I_TRADE_TYPE = 'U' then
        select count(*)
          into T_COUNT
          from DBIPM.I_INT_RATE_SWAP_TB A
         where A.I_INT_RATE_SWAP_CODE = :NEW.I_INVEST_PROD_CODE;
        if T_COUNT = 0 then
          raise_application_error(-20346, DBGLC.G_ERROR_MSG_FC(G_SYS_CODE, -20346, 'Product'));
        end if;
        
        select A.I_NOTIONAL_BAL
          into T_BAL_AMT
          from DBIPM.I_INT_RATE_SWAP_TB A
         where A.I_INT_RATE_SWAP_CODE = :NEW.I_INVEST_PROD_CODE;
        if :NEW.I_QTY > T_BAL_AMT then
           raise_application_error(-20254, DBGLC.G_ERROR_MSG_FC(G_SYS_CODE, -20254, 'Notional'));
        end if;  
      end if;
    end if;
  end if;
end I_PRE_TRADE_TX_BR;
/
